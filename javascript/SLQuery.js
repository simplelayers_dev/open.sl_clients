/**
 * 
 * Created on Jan 21, 2018
 * 
 * @author: Arthur
 */
SimpleLayers.SLQuery = function(sl_client) {
	this.query = {};
	this._memory = {};
	this.INTERSECT_MODE_NONE = 0;
	this.INTERSECT_MODE_INTERSECT = 1;
	this.INTERSECT_MODE_NOT_INTERSECT = -1;
	this.INTERSECT_MODE_WITHIN = 2;
	this.INTERSECT_MODE_NOT_WITHIN = -2;
	
	this.LIKE_STARTS = 'starts';
	this.LIKE_ENDS = 'ends';
	this.LIKE_ANY = 'any';

	this.LIKE_CASE_UPPER = 'upper';
	this.LIKE_CASE_LOWER = 'lower';

	this.STAT_MIN = 'min';
	this.STAT_MAX = 'max';

	this.QUERY_AND = 'and';
	this.QUERY_OR = 'or';

	this.ORDER_DESC = 'desc';
	this.ORDER_ASC = 'asc';

	this.COMPARE_NONE = '';
	this.COMPARE_EQUALS = '==';
	this.COMPARE_NOT_EQUALS = '<>';
	this.COMPARE_GT = '>';
	this.COMPARE_GT_OR_EQUAL = '>=';
	this.COMPARE_LT = '<';
	this.COMPARE_LT_OR_EQUAL = '<=';
	this.COMPARE_HAS = 'contains';
	this.COMPARE_NOT_HAS = '!contains';
	this.COMPARE_ISNULL = 'isnull';
	this.COMPARE_NOT_ISNULL = 'not_isnull';
	this.COMPARE_IN = 'IN';
	this.COMPARE_NOT_IN = 'NOT IN';

	this.BUFFER_MILES = 'mi';
	this.BUFFER_FEET = 'ft';
	this.BUFFER_METERS = 'm';
	this.BUFFER_KILOMETERS = 'km';

	this._groupsBegun = 0;
	this._groupsEnded = 0;
	this._client = sl_client;

	this.NewQuery = function(mapLayerId, mapId,maxCount) {

		this.query = {};
		this.query['token'] = this._client.token;
		this.query['plid'] = mapLayerId;
		this.query['mapId'] = mapId;
		this.Format('json');
		this.query['maxCount'] = maxCount;
		return this;
	};

	this.OrderBy = function(field) {
		this.query['orderby'] = field;
		return this;
	};

	this.Id = function(idField) {
		this.query['idField'] = idField;
		return this;
	};

	this.Layer = function(plid) {
		this.query['plid'] = plid;
		return this;
	};

	this.Project = function(projectId) {
		this.query['project'] = projectId;
		return this;
	};

	this.Limit = function(limit) {
		this.query['limit'] = limit;
		return this;
	};

	this.First = function(first) {
		this.query['first'] = first;
		return this;
	};

	this.Format = function(fmt) {
		this.query['format'] = fmt;
		return this;
	};

	this.AddSort = function(field, opt_direction, opt_fieldType) {
		if (!this.query.hasOwnProperty("sort")) {
			this.query['sort'] = [];
		}

		var sortInfo = {};
		sortInfo['field'] = field;

		if (typeof opt_fieldType != 'undefined') {
			sortInfo['type'] = fieldType;
		}

		if (typeof opt_direction != 'undefined') {
			sortInfo['direction'] = direction;
		}
		this.query['sort'].push(sortInfo);
		return this;
	};

	this.AddUpperSort = function(field, opt_direction, opt_fieldType) {
		this.AddSort('upper:' + field, opt_direction, opt_fieldType);
		return this;
	};

	this.AddLowerSort = function(field, opt_direction, opt_fieldType) {
		this.AddSort('lower:' + field, opt_direction, opt_fieldType);
		return this;
	};

	this.BeginCriteriaGroup = function(opt_andOr) {
		this._groupsBegun += 1;
		var criteria = {};
		if (typeof opt_andOr != 'undefined') {
			criteria['andor'] = opt_andOr.toLowerCase();
		}
		criteria['group'] = this._groupsBegun;
		if (!this.query.hasOwnProperty('criteria')) {
			this.query['criteria'] = [];
		}
		this.query['criteria'].push(criteria);
		return this;
	};

	this.EndCriteriaGroup = function() {
		this._groupsEnded += 1;
		var criteria = {};
		criteria['!group'] = this._groupsEnded;
		if (!this.query.hasOwnProperty('criteria')) {
			this.query['criteria'] = [];
		}
		this.query['criteria'].push(criteria);
		return this;
	};

	this.GetResults = function(handler, justResults, debug) {
		if (justResults === undefined)
			justResults = true;
		if (debug === undefined)
			debug = true;

		this.query['token'] = this._client.token;

		if (this._groupsBegun != this._groupsEnded) {
			throw 'SLQuery::GetResults error: Groups begun does not mathc Groups ended.';
		}
		this._client.debug = debug;
		var params = {};
		params.json_params = JSON.stringify(this.query);
		console.log(this.query);
		this._client.PostEndPoint('features/find/mode:search/', params, handler,justResults);
		return this;
	};

	this.GetValues = function(srcArray, opt_field) {
		var values;
		values = [];

		for ( var idx in srcArray) {
			var item = srcArray[idx];

			if (typeof opt_field == "undefined") {
				return srcArray;
			}
			if (item[opt_field] === "undefined") {
				continue;
			}
			values.push(item[opt_field]);
		}
		return values;
	};

	this.GetKeyValues = function(srcArray, keyField, opt_valField) {
		var values = {};

		for ( var idx in srcArray) {
			var item = srcArray[idx];
			if (typeof idx === 'number') {
				return this.GetKeyValues([ srcArray ], keyField, valField);
			}
			var itemKey = null;
			if (item.hasOwnProperty(keyField)) {
				itemKey = item[keyField];
			}
			if (itemKey === null) {
				continue;
			}
			if (typeof opt_valField == "undefined") {
				values[itemKey] = item;
				continue;
			}

			if (item[keyField] === undefined) {
				continue;
			}
			values[item[keyField]] = item[valField];
		}
		return values;

	};

	this.GetUniqueValues = function(haystack, field, opt_sortResult) {
		if (typeof opt_sortResult === "undefined") {
			opt_sortResult = true;
		}
		if (haystack.hasOwnProperty(field)) {
			return [ haystack[field] ];
		}
		var values = {};
		for ( var i in haystack) {
			var item = haystack[i];
			if (item.hasOwnProperty(field)) {
				continue;
			}
			if (values.indexOf(item[field]) > -1) {
				continue;
			}
			values.push(item[field]);
		}
		if (opt_sortResult) {
			values.sort();
		}
		return values;
	};
	this.AddDistinctField = function(field, opt_as, opt_dataType) {
		if (!this.query.hasOwnProperty('fields')) {
			this.query['fields'] = [];
		}

		var fieldData = {};
		fieldData.field = field;
		fieldData.distinct = 1;

		if (opt_as !== undefined) {
			fieldData['as'] = opt_as;
		}
		if (opt_dataType !== undefined) {
			fieldData['type'] = opt_dataType;
		}
		this.query['fields'].push(fieldData);
		return this;
	};

	this.AddFields = function() {
		var fields = arguments;
		for ( var i in fields) {
			var field = fields[i];
			this.AddField(field);
		}
		return this;
	};

	this.AddAllFields = function() {
		this.AddField('*');
		return this;
	};

	this.AddField = function(field, opt_as, opt_dataType) {
		if (!this.query.hasOwnProperty('fields')) {
			this.query['fields'] = [];
		}
		var fieldData = {};
		fieldData.field = field;

		if (opt_as !== undefined) {
			fieldData['as'] = opt_as;
		}
		if (opt_dataType !== undefined) {
			fieldData['type'] = opt_dataType;
		}
		this.query['fields'].push(fieldData);
		return this;
	};

	this.AddUpperField = function(field, opt_as) {
		this.AddField(field, opt_as, 'upper');
		return this;
	};

	this.AddLowerField = function(field, opt_as) {
		this.AddField(field, opt_as, 'lower');
		return this;
	};

	this.AddLimit = function(limit) {
		this.query['limit'] = limit;
		return this;
	};

	this.AddStat = function(field, statType, fieldAs) {
		if (!this.query.hasOwnProperty('stats')) {
			this.query['stats'] = [];
		}
		var stat = {};
		stat.field = field;
		stat.stat = statType;
		stat.as = fieldAs;

		this.query.stats.push(stat);
		return this;
	};

	this.PrecheckCriteria = function() {
		if (!this.query.hasOwnProperty('criteria')) {
			this.query['criteria'] = [];
		}
	};

	this.AddIsNullCriteria = function(field, opt_andOr) {
		this.PrecheckCriteria();
		var criteria = {};
		criteria.compare = this.COMPARE_ISNULL;
		criteria.value = '';

		this.AddCriteriaField(criteria, field);
		if (opt_andOr !== undefined) {
			criteria['andor'] = opt_andOr.toLowerCase();
		}
		this.query['criteria'].push(criteria);
		return this;
	};

	this.AddIsNotNullCriteria = function(field, opt_andOr) {
		this.PrecheckCriteria();
		criteria = {};
		criteria.compare = this.COMPARE_NOT_ISNULL;
		criteria.value = '';
		this.AddCriteriaField(criteria, field);

		if (opt_andOr !== undefined) {
			criteria['andor'] = opt_andOr.toLowerCase();
		}
		this.query['criteria'].push(criteria);
		return this;
	};

	this.AddInCriteria = function(field, values, opt_quoteVals, opt_andOr) {
		if (opt_quoteVals === undefined)
			opt_quoteVals = true;
		this.PrecheckCriteria();

		if (opt_quoteVals) {
			var valsQuoted = [];
			for ( var i in values) {
				var value = values[i];
				valsQuoted.push("'" + value + "'");
			}
			values = valsQuoted.join(',');
		} else {
			values = values.join(',');
		}
		var criteria = {
			'compare' : this.COMPARE_IN,
			'value' : values
		};

		this.AddCriteriaField(criteria, field);
		if (opt_andOr !== undefined) {
			criteria['andor'] = opt_andOr.toLowerCase();
		}
		this.query['criteria'].push(criteria);
		return this;
	};

	this.AddCriteriaField = function(criteria, field) {
		var fieldInfo = field.split(':');
		var field = fieldInfo.pop();

		criteria['field'] = field;
		if (fieldInfo.length > 0) {
			criteria['modifier'] = fieldInfo.join(':');
		}
		return this;
	};

	this.AddLikeCriteria = function(field, value, pos, opt_andOr) {
		if (pos === undefined)
			pos = 'start';
		this.PrecheckCriteria();

		switch (pos) {
		case 'starts':
		case 'start':
		case this.LIKE_STARTS:
			value += '%';
			break;
		case 'any':
		case this.LIKE_ANY:
			value = '%' + value + '%';
			break;
		case 'ends':
		case 'end':
		case this.LIKE_ENDS:
			value = '%' + value;
			break;
		}
		var criteria = {
			'compare' : this.COMPARE_HAS,
			'value' : value
		};

		this.AddCriteriaField(criteria, field);
		if (opt_andOr !== undefined) {
			criteria['andor'] = opt_andOr.toLowerCase();
		}
		this.query['criteria'].push(criteria);
		return this;
	};

	this.AddNotLikeCriteria = function(field, value, opt_pos, opt_andOr) {
		if (opt_pos === undefined) {
			opt_pos = this.LIKE_ANY;
		}
		this.PrecheckCriteria();

		switch (pos) {
		case this.LIKE_STARTS:
			value += '%';
			break;
		case this.LIKE_ANY:
			value = '%' + value + '%';
			break;
		case this.LIKE_ENDS:
			value = '%' + value;
			break;
		}

		var criteria = {
			'compare' : this.COMPARE_NOT_HAS,
			'value' : value
		};
		if (opt_andOr !== undefined) {
			criteria['andor'] = andOr.toLowerCase();
		}
		this.AddCriteriaField(criteria, field);
		this.query['criteria'].push(criteria);
		return this;
	};

	this.AddDateRangeCriteria = function(field, mintime, maxtime, opt_andOr) {
		this.PrecheckCriteria();
		var criteria = {
			'minunixtime' : mintime,
			'maxunixtime' : maxtime
		};
		this.AddCriteriaField(criteria, field);
		if (opt_andOr !== undefined) {
			criteria['andor'] = opt_andOr.toLowerCase();
		}
		this.query['criteria'].push(criteria);
		return this;
	};

	this.AddEqualsCriteria = function(field, opt_value, opt_andOr) {
		if (opt_value === undefined) {
			return this.AddIsNullCriteria(field, andOr);
		}
		if (opt_value === null) {
			return this.AddIsNullCriteria(field, andOr);
		}

		this.PrecheckCriteria();
		var criteria = {
			'compare' : this.COMPARE_EQUALS,
			'value' : opt_value
		};
		this.AddCriteriaField(criteria, field);
		if (opt_andOr !== undefined) {
			criteria['andor'] = opt_andOr.toLowerCase();
		}
		this.query['criteria'].push(criteria);
		return this;
	};

	this.AddNotEqualsCriteria = function(field, opt_value, opt_andOr) {
		if (opt_value === undefined) {
			return this.AddIsNotNullCriteria(field, andOr);
		}

		if (opt_value === null) {
			return this.AddIsNotNullCriteria(field, andOr);
		}

		this.PrecheckCriteria();
		var criteria = {
			'field' : field,
			'compare' : this.COMPARE_NOT_EQUALS,
			'value' : value
		};
		this.AddCriteriaField(criteria, field);

		if (opt_andOr != undefined) {
			criteria['andor'] = opt_andOr.toLower();
		}
		this.query['criteria'].push(criteria);
		return this;
	};

	this.AddGroup = function(field) {
		if (!this.query.hasOwnProperty('groups')) {
			this.query['groups'] = [];
		}
		this.query['groups'].push(field);
		return this;
	};

	this.AddUpperGroup = function(field) {
		this.AddGroup('upper:' + field);
		return this;
	};

	this.AddLowerGroup = function(field) {
		this.AddGroup('lower:' + field);
		return this;
	};

	this.Remember = function(memoryMapLayer, gids) {
		this._memory = {
			'memoryMapLayer' : memoryMapLayer,
			'gids' : gids
		};
		return this;
	};

	this.Intersect = function(intersectMode) {
		if (this._memory.hasOwnProperty('memoryMapLayer')) {
			this.query.memoryMapLayer = this._memory.memoryMapLayer;
		}
		if (this._memory.hasOwnProperty('gids')) {
			this.query.gids = this._memory.gids;
		}
		this.query.intersectMode = intersectMode;
		return this;
	};

	this.SetQueryArea = function(bbox) {
		this.query.bbox = bbox;
		return this;
	};

	this.IncludeGeom = function() {
		this.query.geom=true;
	};
	this.ExcludeGeom = function() {
		if(this.query.hasOwnProperty('geom')) {
			delete this.query['geom'];
		}
	};
	this.DetectQueryArea = function(bbox, pbox, viewWidth, viewHeight) {
		/**
		 * Legacy, based on ROIs set in lat/lon map rather than spherical mercator.
		 * 
		 * If you are using leaflet or other web maps, use SetQueryArea and MakeBBOX
		 * to constrain searches to an area.
		 */
		this.query.bbox = bbox;
		this.query.pbox = pbox;
		this.query.width = viewWidth;
		this.query.height = viewHeight;
		return this;
	};

	this.SetBuffer = function(distance, opt_units) {
		if (opt_units === undefined)
			opt_units == this.BUFFER_METERS;
		switch (opt_units) {
		case this.BUFFER_METERS:
			this.query['buffer'] = distance;
			break;
		case this.BUFFER_KILOMETERS:
			this.query['buffer'] = distance * 1000.00;
			break;
		case this.BUFFER_MILES:
			this.query['buffer'] = distance / 0.000621371;
			break;
		case this.BUFFER_FEET:
			this.query['buffer'] = distance / 3.2808399;
			break;
		}
		return this;
	};
	this.MakeBBOX = function(minLon, minLat, maxLon, maxLat) {
		var bbox = [ minLon, minLat, maxLon, maxLat ].join(',');
		return bbox;
	};
	this.MakePxBox = function(x1, y1, x2, y2) {
		var box = [ x1, y1, x2, y2 ].join(',');
		return box;
	};
	this.is_number = function(testVar) {
		return typeof testVar === 'number';
	};
};
