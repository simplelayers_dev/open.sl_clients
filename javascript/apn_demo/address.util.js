/**
 * 
 */
SimpleLayers.AddressUtil = function() {
	this.abbrevs = {};
	this.abbrevs["ALLEE"]="ALY";
	this.abbrevs["ALLEY"]="ALY";
	this.abbrevs["ALLY"]="ALY";
	this.abbrevs["ALY"]="ALY";
	this.abbrevs["ANEX"]="ANX";
	this.abbrevs["ANNEX"]="ANX";
	this.abbrevs["ANNX"]="ANX";
	this.abbrevs["ANX"]="ANX";
	this.abbrevs["ARC"]="ARC";
	this.abbrevs["ARCADE"]="ARC";
	this.abbrevs["AV"]="AVE";
	this.abbrevs["AVE"]="AVE";
	this.abbrevs["AVEN"]="AVE";
	this.abbrevs["AVENU"]="AVE";
	this.abbrevs["AVENUE"]="AVE";
	this.abbrevs["AVN"]="AVE";
	this.abbrevs["AVNUE"]="AVE";
	this.abbrevs["BAYOO"]="BYU";
	this.abbrevs["BAYOU"]="BYU";
	this.abbrevs["BCH"]="BCH";
	this.abbrevs["BEACH"]="BCH";
	this.abbrevs["BEND"]="BND";
	this.abbrevs["BND"]="BND";
	this.abbrevs["BLF"]="BLF";
	this.abbrevs["BLUF"]="BLF";
	this.abbrevs["BLUFF"]="BLF";
	this.abbrevs["BLUFFS"]="BLFS";
	this.abbrevs["BOT"]="BTM";
	this.abbrevs["BTM"]="BTM";
	this.abbrevs["BOTTM"]="BTM";
	this.abbrevs["BOTTOM"]="BTM";
	this.abbrevs["BLVD"]="BLVD";
	this.abbrevs["BOUL"]="BLVD";
	this.abbrevs["BOULEVARD"]="BLVD";
	this.abbrevs["BOULV"]="BLVD";
	this.abbrevs["BR"]="BR";
	this.abbrevs["BRNCH"]="BR";
	this.abbrevs["BRANCH"]="BR";
	this.abbrevs["BRDGE"]="BRG";
	this.abbrevs["BRG"]="BRG";
	this.abbrevs["BRIDGE"]="BRG";
	this.abbrevs["BRK"]="BRK";
	this.abbrevs["BROOK"]="BRK";
	this.abbrevs["BROOKS"]="BRKS";
	this.abbrevs["BURG"]="BG";
	this.abbrevs["BURGS"]="BGS";
	this.abbrevs["BYP"]="BYP";
	this.abbrevs["BYPA"]="BYP";
	this.abbrevs["BYPAS"]="BYP";
	this.abbrevs["BYPASS"]="BYP";
	this.abbrevs["BYPS"]="BYP";
	this.abbrevs["CAMP"]="CP";
	this.abbrevs["CP"]="CP";
	this.abbrevs["CMP"]="CP";
	this.abbrevs["CANYN"]="CYN";
	this.abbrevs["CANYON"]="CYN";
	this.abbrevs["CNYN"]="CYN";
	this.abbrevs["CAPE"]="CPE";
	this.abbrevs["CPE"]="CPE";
	this.abbrevs["CAUSEWAY"]="CSWY";
	this.abbrevs["CAUSWA"]="CSWY";
	this.abbrevs["CSWY"]="CSWY";
	this.abbrevs["CEN"]="CTR";
	this.abbrevs["CENT"]="CTR";
	this.abbrevs["CENTER"]="CTR";
	this.abbrevs["CENTR"]="CTR";
	this.abbrevs["CENTRE"]="CTR";
	this.abbrevs["CNTER"]="CTR";
	this.abbrevs["CNTR"]="CTR";
	this.abbrevs["CTR"]="CTR";
	this.abbrevs["CENTERS"]="CTRS";
	this.abbrevs["CIR"]="CIR";
	this.abbrevs["CIRC"]="CIR";
	this.abbrevs["CIRCL"]="CIR";
	this.abbrevs["CIRCLE"]="CIR";
	this.abbrevs["CRCL"]="CIR";
	this.abbrevs["CRCLE"]="CIR";
	this.abbrevs["CIRCLES"]="CIRS";
	this.abbrevs["CLF"]="CLF";
	this.abbrevs["CLIFF"]="CLF";
	this.abbrevs["CLFS"]="CLFS";
	this.abbrevs["CLIFFS"]="CLFS";
	this.abbrevs["CLB"]="CLB";
	this.abbrevs["CLUB"]="CLB";
	this.abbrevs["COMMON"]="CMN";
	this.abbrevs["COMMONS"]="CMNS";
	this.abbrevs["COR"]="COR";
	this.abbrevs["CORNER"]="COR";
	this.abbrevs["CORNERS"]="CORS";
	this.abbrevs["CORS"]="CORS";
	this.abbrevs["COURSE"]="CRSE";
	this.abbrevs["CRSE"]="CRSE";
	this.abbrevs["COURT"]="CT";
	this.abbrevs["CT"]="CT";
	this.abbrevs["COURTS"]="CTS";
	this.abbrevs["CTS"]="CTS";
	this.abbrevs["COVE"]="CV";
	this.abbrevs["CV"]="CV";
	this.abbrevs["COVES"]="CVS";
	this.abbrevs["CREEK"]="CRK";
	this.abbrevs["CRK"]="CRK";
	this.abbrevs["CRESCENT"]="CRES";
	this.abbrevs["CRES"]="CRES";
	this.abbrevs["CRSENT"]="CRES";
	this.abbrevs["CRSNT"]="CRES";
	this.abbrevs["CREST"]="CRST";
	this.abbrevs["CROSSING"]="XING";
	this.abbrevs["CRSSNG"]="XING";
	this.abbrevs["XING"]="XING";
	this.abbrevs["CROSSROAD"]="XRD";
	this.abbrevs["CROSSROADS"]="XRDS";
	this.abbrevs["CURVE"]="CURV";
	this.abbrevs["DALE"]="DL";
	this.abbrevs["DL"]="DL";
	this.abbrevs["DAM"]="DM";
	this.abbrevs["DM"]="DM";
	this.abbrevs["DIV"]="DV";
	this.abbrevs["DIVIDE"]="DV";
	this.abbrevs["DV"]="DV";
	this.abbrevs["DVD"]="DV";
	this.abbrevs["DR"]="DR";
	this.abbrevs["DRIV"]="DR";
	this.abbrevs["DRIVE"]="DR";
	this.abbrevs["DRV"]="DR";
	this.abbrevs["DRIVES"]="DRS";
	this.abbrevs["EST"]="EST";
	this.abbrevs["ESTATE"]="EST";
	this.abbrevs["ESTATES"]="ESTS";
	this.abbrevs["ESTS"]="ESTS";
	this.abbrevs["EXP"]="EXPY";
	this.abbrevs["EXPR"]="EXPY";
	this.abbrevs["EXPRESS"]="EXPY";
	this.abbrevs["EXPRESSWAY"]="EXPY";
	this.abbrevs["EXPW"]="EXPY";
	this.abbrevs["EXPY"]="EXPY";
	this.abbrevs["EXT"]="EXT";
	this.abbrevs["EXTENSION"]="EXT";
	this.abbrevs["EXTN"]="EXT";
	this.abbrevs["EXTNSN"]="EXT";
	this.abbrevs["EXTS"]="EXTS";
	this.abbrevs["FALL"]="FALL";
	this.abbrevs["FALLS"]="FLS";
	this.abbrevs["FLS"]="FLS";
	this.abbrevs["FERRY"]="FRY";
	this.abbrevs["FRRY"]="FRY";
	this.abbrevs["FRY"]="FRY";
	this.abbrevs["FIELD"]="FLD";
	this.abbrevs["FLD"]="FLD";
	this.abbrevs["FIELDS"]="FLDS";
	this.abbrevs["FLDS"]="FLDS";
	this.abbrevs["FLAT"]="FLT";
	this.abbrevs["FLT"]="FLT";
	this.abbrevs["FLATS"]="FLTS";
	this.abbrevs["FLTS"]="FLTS";
	this.abbrevs["FORD"]="FRD";
	this.abbrevs["FRD"]="FRD";
	this.abbrevs["FORDS"]="FRDS";
	this.abbrevs["FOREST"]="FRST";
	this.abbrevs["FORESTS"]="FRST";
	this.abbrevs["FRST"]="FRST";
	this.abbrevs["FORG"]="FRG";
	this.abbrevs["FORGE"]="FRG";
	this.abbrevs["FRG"]="FRG";
	this.abbrevs["FORGES"]="FRGS";
	this.abbrevs["FORK"]="FRK";
	this.abbrevs["FRK"]="FRK";
	this.abbrevs["FORKS"]="FRKS";
	this.abbrevs["FRKS"]="FRKS";
	this.abbrevs["FORT"]="FT";
	this.abbrevs["FRT"]="FT";
	this.abbrevs["FT"]="FT";
	this.abbrevs["FREEWAY"]="FWY";
	this.abbrevs["FREEWY"]="FWY";
	this.abbrevs["FRWAY"]="FWY";
	this.abbrevs["FRWY"]="FWY";
	this.abbrevs["FWY"]="FWY";
	this.abbrevs["GARDEN"]="GDN";
	this.abbrevs["GARDN"]="GDN";
	this.abbrevs["GRDEN"]="GDN";
	this.abbrevs["GRDN"]="GDN";
	this.abbrevs["GARDENS"]="GDNS";
	this.abbrevs["GDNS"]="GDNS";
	this.abbrevs["GRDNS"]="GDNS";
	this.abbrevs["GATEWAY"]="GTWY";
	this.abbrevs["GATEWY"]="GTWY";
	this.abbrevs["GATWAY"]="GTWY";
	this.abbrevs["GTWAY"]="GTWY";
	this.abbrevs["GTWY"]="GTWY";
	this.abbrevs["GLEN"]="GLN";
	this.abbrevs["GLN"]="GLN";
	this.abbrevs["GLENS"]="GLNS";
	this.abbrevs["GREEN"]="GRN";
	this.abbrevs["GRN"]="GRN";
	this.abbrevs["GREENS"]="GRNS";
	this.abbrevs["GROV"]="GRV";
	this.abbrevs["GROVE"]="GRV";
	this.abbrevs["GRV"]="GRV";
	this.abbrevs["GROVES"]="GRVS";
	this.abbrevs["HARB"]="HBR";
	this.abbrevs["HARBOR"]="HBR";
	this.abbrevs["HARBR"]="HBR";
	this.abbrevs["HBR"]="HBR";
	this.abbrevs["HRBOR"]="HBR";
	this.abbrevs["HARBORS"]="HBRS";
	this.abbrevs["HAVEN"]="HVN";
	this.abbrevs["HVN"]="HVN";
	this.abbrevs["HT"]="HTS";
	this.abbrevs["HTS"]="HTS";
	this.abbrevs["HIGHWAY"]="HWY";
	this.abbrevs["HIGHWY"]="HWY";
	this.abbrevs["HIWAY"]="HWY";
	this.abbrevs["HIWY"]="HWY";
	this.abbrevs["HWAY"]="HWY";
	this.abbrevs["HWY"]="HWY";
	this.abbrevs["HILL"]="HL";
	this.abbrevs["HL"]="HL";
	this.abbrevs["HILLS"]="HLS";
	this.abbrevs["HLS"]="HLS";
	this.abbrevs["HLLW"]="HOLW";
	this.abbrevs["HOLLOW"]="HOLW";
	this.abbrevs["HOLLOWS"]="HOLW";
	this.abbrevs["HOLW"]="HOLW";
	this.abbrevs["HOLWS"]="HOLW";
	this.abbrevs["INLT"]="INLT";
	this.abbrevs["IS"]="IS";
	this.abbrevs["ISLAND"]="IS";
	this.abbrevs["ISLND"]="IS";
	this.abbrevs["ISLANDS"]="ISS";
	this.abbrevs["ISLNDS"]="ISS";
	this.abbrevs["ISS"]="ISS";
	this.abbrevs["ISLE"]="ISLE";
	this.abbrevs["ISLES"]="ISLE";
	this.abbrevs["JCT"]="JCT";
	this.abbrevs["JCTION"]="JCT";
	this.abbrevs["JCTN"]="JCT";
	this.abbrevs["JUNCTION"]="JCT";
	this.abbrevs["JUNCTN"]="JCT";
	this.abbrevs["JUNCTON"]="JCT";
	this.abbrevs["JCTNS"]="JCTS";
	this.abbrevs["JCTS"]="JCTS";
	this.abbrevs["JUNCTIONS"]="JCTS";
	this.abbrevs["KEY"]="KY";
	this.abbrevs["KY"]="KY";
	this.abbrevs["KEYS"]="KYS";
	this.abbrevs["KYS"]="KYS";
	this.abbrevs["KNL"]="KNL";
	this.abbrevs["KNOL"]="KNL";
	this.abbrevs["KNOLL"]="KNL";
	this.abbrevs["KNLS"]="KNLS";
	this.abbrevs["KNOLLS"]="KNLS";
	this.abbrevs["LK"]="LK";
	this.abbrevs["LAKE"]="LK";
	this.abbrevs["LKS"]="LKS";
	this.abbrevs["LAKES"]="LKS";
	this.abbrevs["LAND"]="LAND";
	this.abbrevs["LANDING"]="LNDG";
	this.abbrevs["LNDG"]="LNDG";
	this.abbrevs["LNDNG"]="LNDG";
	this.abbrevs["LANE"]="LN";
	this.abbrevs["LN"]="LN";
	this.abbrevs["LGT"]="LGT";
	this.abbrevs["LIGHT"]="LGT";
	this.abbrevs["LIGHTS"]="LGTS";
	this.abbrevs["LF"]="LF";
	this.abbrevs["LOAF"]="LF";
	this.abbrevs["LCK"]="LCK";
	this.abbrevs["LOCK"]="LCK";
	this.abbrevs["LCKS"]="LCKS";
	this.abbrevs["LOCKS"]="LCKS";
	this.abbrevs["LDG"]="LDG";
	this.abbrevs["LDGE"]="LDG";
	this.abbrevs["LODG"]="LDG";
	this.abbrevs["LODGE"]="LDG";
	this.abbrevs["LOOP"]="LOOP";
	this.abbrevs["LOOPS"]="LOOP";
	this.abbrevs["MALL"]="MALL";
	this.abbrevs["MNR"]="MNR";
	this.abbrevs["MANOR"]="MNR";
	this.abbrevs["MANORS"]="MNRS";
	this.abbrevs["MNRS"]="MNRS";
	this.abbrevs["MEADOW"]="MDW";
	this.abbrevs["MDW"]="MDWS";
	this.abbrevs["MDWS"]="MDWS";
	this.abbrevs["MEADOWS"]="MDWS";
	this.abbrevs["MEDOWS"]="MDWS";
	this.abbrevs["MEWS"]="MEWS";
	this.abbrevs["MILL"]="ML";
	this.abbrevs["MILLS"]="MLS";
	this.abbrevs["MISSN"]="MSN";
	this.abbrevs["MSSN"]="MSN";
	this.abbrevs["MOTORWAY"]="MTWY";
	this.abbrevs["MNT"]="MT";
	this.abbrevs["MT"]="MT";
	this.abbrevs["MOUNT"]="MT";
	this.abbrevs["MNTAIN"]="MTN";
	this.abbrevs["MNTN"]="MTN";
	this.abbrevs["MOUNTAIN"]="MTN";
	this.abbrevs["MOUNTIN"]="MTN";
	this.abbrevs["MTIN"]="MTN";
	this.abbrevs["MTN"]="MTN";
	this.abbrevs["MNTNS"]="MTNS";
	this.abbrevs["MOUNTAINS"]="MTNS";
	this.abbrevs["NCK"]="NCK";
	this.abbrevs["NECK"]="NCK";
	this.abbrevs["ORCH"]="ORCH";
	this.abbrevs["ORCHARD"]="ORCH";
	this.abbrevs["ORCHRD"]="ORCH";
	this.abbrevs["OVAL"]="OVAL";
	this.abbrevs["OVL"]="OVAL";
	this.abbrevs["OVERPASS"]="OPAS";
	this.abbrevs["PARK"]="PARK";
	this.abbrevs["PRK"]="PARK";
	this.abbrevs["PARKS"]="PARK";
	this.abbrevs["PARKWAY"]="PKWY";
	this.abbrevs["PARKWY"]="PKWY";
	this.abbrevs["PKWAY"]="PKWY";
	this.abbrevs["PKWY"]="PKWY";
	this.abbrevs["PKY"]="PKWY";
	this.abbrevs["PARKWAYS"]="PKWY";
	this.abbrevs["PKWYS"]="PKWY";
	this.abbrevs["PASS"]="PASS";
	this.abbrevs["PASSAGE"]="PSGE";
	this.abbrevs["PATH"]="PATH";
	this.abbrevs["PATHS"]="PATH";
	this.abbrevs["PIKE"]="PIKE";
	this.abbrevs["PIKES"]="PIKE";
	this.abbrevs["PINE"]="PNE";
	this.abbrevs["PINES"]="PNES";
	this.abbrevs["PNES"]="PNES";
	this.abbrevs["PL"]="PL";
	this.abbrevs["PLAIN"]="PLN";
	this.abbrevs["PLN"]="PLN";
	this.abbrevs["PLAINS"]="PLNS";
	this.abbrevs["PLNS"]="PLNS";
	this.abbrevs["PLAZA"]="PLZ";
	this.abbrevs["PLZ"]="PLZ";
	this.abbrevs["PLZA"]="PLZ";
	this.abbrevs["POINT"]="PT";
	this.abbrevs["PT"]="PT";
	this.abbrevs["POINTS"]="PTS";
	this.abbrevs["PTS"]="PTS";
	this.abbrevs["PORT"]="PRT";
	this.abbrevs["PRT"]="PRT";
	this.abbrevs["PORTS"]="PRTS";
	this.abbrevs["PRTS"]="PRTS";
	this.abbrevs["PR"]="PR";
	this.abbrevs["PRAIRIE"]="PR";
	this.abbrevs["PRR"]="PR";
	this.abbrevs["RAD"]="RADL";
	this.abbrevs["RADIAL"]="RADL";
	this.abbrevs["RADIEL"]="RADL";
	this.abbrevs["RADL"]="RADL";
	this.abbrevs["RAMP"]="RAMP";
	this.abbrevs["RANCH"]="RNCH";
	this.abbrevs["RANCHES"]="RNCH";
	this.abbrevs["RNCH"]="RNCH";
	this.abbrevs["RNCHS"]="RNCH";
	this.abbrevs["RAPID"]="RPD";
	this.abbrevs["RPD"]="RPD";
	this.abbrevs["RAPIDS"]="RPDS";
	this.abbrevs["RPDS"]="RPDS";
	this.abbrevs["REST"]="RST";
	this.abbrevs["RST"]="RST";
	this.abbrevs["RDG"]="RDG";
	this.abbrevs["RDGE"]="RDG";
	this.abbrevs["RIDGE"]="RDG";
	this.abbrevs["RDGS"]="RDGS";
	this.abbrevs["RIDGES"]="RDGS";
	this.abbrevs["RIV"]="RIV";
	this.abbrevs["RIVER"]="RIV";
	this.abbrevs["RVR"]="RIV";
	this.abbrevs["RIVR"]="RIV";
	this.abbrevs["RD"]="RD";
	this.abbrevs["ROAD"]="RD";
	this.abbrevs["ROADS"]="RDS";
	this.abbrevs["RDS"]="RDS";
	this.abbrevs["ROUTE"]="RTE";
	this.abbrevs["ROW"]="ROW";
	this.abbrevs["RUE"]="RUE";
	this.abbrevs["RUN"]="RUN";
	this.abbrevs["SHL"]="SHL";
	this.abbrevs["SHOAL"]="SHL";
	this.abbrevs["SHLS"]="SHLS";
	this.abbrevs["SHOALS"]="SHLS";
	this.abbrevs["SHOAR"]="SHR";
	this.abbrevs["SHORE"]="SHR";
	this.abbrevs["SHR"]="SHR";
	this.abbrevs["SHOARS"]="SHRS";
	this.abbrevs["SHORES"]="SHRS";
	this.abbrevs["SHRS"]="SHRS";
	this.abbrevs["SKYWAY"]="SKWY";
	this.abbrevs["SPG"]="SPG";
	this.abbrevs["SPNG"]="SPG";
	this.abbrevs["SPRING"]="SPG";
	this.abbrevs["SPRNG"]="SPG";
	this.abbrevs["SPGS"]="SPGS";
	this.abbrevs["SPNGS"]="SPGS";
	this.abbrevs["SPRINGS"]="SPGS";
	this.abbrevs["SPRNGS"]="SPGS";
	this.abbrevs["SPUR"]="SPUR";
	this.abbrevs["SPURS"]="SPUR";
	this.abbrevs["SQ"]="SQ";
	this.abbrevs["SQR"]="SQ";
	this.abbrevs["SQRE"]="SQ";
	this.abbrevs["SQU"]="SQ";
	this.abbrevs["SQUARE"]="SQ";
	this.abbrevs["SQRS"]="SQS";
	this.abbrevs["SQUARES"]="SQS";
	this.abbrevs["STA"]="STA";
	this.abbrevs["STATION"]="STA";
	this.abbrevs["STATN"]="STA";
	this.abbrevs["STN"]="STA";
	this.abbrevs["STRA"]="STRA";
	this.abbrevs["STRAV"]="STRA";
	this.abbrevs["STRAVEN"]="STRA";
	this.abbrevs["STRAVENUE"]="STRA";
	this.abbrevs["STRAVN"]="STRA";
	this.abbrevs["STRVN"]="STRA";
	this.abbrevs["STRVNUE"]="STRA";
	this.abbrevs["STREAM"]="STRM";
	this.abbrevs["STREME"]="STRM";
	this.abbrevs["STRM"]="STRM";
	this.abbrevs["STREET"]="ST";
	this.abbrevs["STRT"]="ST";
	this.abbrevs["ST"]="ST";
	this.abbrevs["STR"]="ST";
	this.abbrevs["STREETS"]="STS";
	this.abbrevs["SMT"]="SMT";
	this.abbrevs["SUMIT"]="SMT";
	this.abbrevs["SUMITT"]="SMT";
	this.abbrevs["SUMMIT"]="SMT";
	this.abbrevs["TER"]="TER";
	this.abbrevs["TERR"]="TER";
	this.abbrevs["TERRACE"]="TER";
	this.abbrevs["THROUGHWAY"]="TRWY";
	this.abbrevs["TRACE"]="TRCE";
	this.abbrevs["TRACES"]="TRCE";
	this.abbrevs["TRCE"]="TRCE";
	this.abbrevs["TRACK"]="TRAK";
	this.abbrevs["TRACKS"]="TRAK";
	this.abbrevs["TRAK"]="TRAK";
	this.abbrevs["TRK"]="TRAK";
	this.abbrevs["TRKS"]="TRAK";
	this.abbrevs["TRAFFICWAY"]="TRFY";
	this.abbrevs["TRAIL"]="TRL";
	this.abbrevs["TRAILS"]="TRL";
	this.abbrevs["TRL"]="TRL";
	this.abbrevs["TRLS"]="TRL";
	this.abbrevs["TRAILER"]="TRLR";
	this.abbrevs["TRLR"]="TRLR";
	this.abbrevs["TRLRS"]="TRLR";
	this.abbrevs["TUNEL"]="TUNL";
	this.abbrevs["TUNL"]="TUNL";
	this.abbrevs["TUNLS"]="TUNL";
	this.abbrevs["TUNNEL"]="TUNL";
	this.abbrevs["TUNNELS"]="TUNL";
	this.abbrevs["TUNNL"]="TUNL";
	this.abbrevs["TRNPK"]="TPKE";
	this.abbrevs["TURNPIKE"]="TPKE";
	this.abbrevs["TURNPK"]="TPKE";
	this.abbrevs["UNDERPASS"]="UPAS";
	this.abbrevs["UN"]="UN";
	this.abbrevs["UNION"]="UN";
	this.abbrevs["UNIONS"]="UNS";
	this.abbrevs["VALLEY"]="VLY";
	this.abbrevs["VALLY"]="VLY";
	this.abbrevs["VLLY"]="VLY";
	this.abbrevs["VLY"]="VLY";
	this.abbrevs["VALLEYS"]="VLYS";
	this.abbrevs["VLYS"]="VLYS";
	this.abbrevs["VDCT"]="VIA";
	this.abbrevs["VIA"]="VIA";
	this.abbrevs["VIADCT"]="VIA";
	this.abbrevs["VIADUCT"]="VIA";
	this.abbrevs["VIEW"]="VW";
	this.abbrevs["VW"]="VW";
	this.abbrevs["VIEWS"]="VWS";
	this.abbrevs["VWS"]="VWS";
	this.abbrevs["VILL"]="VLG";
	this.abbrevs["VILLAG"]="VLG";
	this.abbrevs["VILLAGE"]="VLG";
	this.abbrevs["VILLG"]="VLG";
	this.abbrevs["VILLIAGE"]="VLG";
	this.abbrevs["VLG"]="VLG";
	this.abbrevs["VILLAGES"]="VLGS";
	this.abbrevs["VLGS"]="VLGS";
	this.abbrevs["VILLE"]="VL";
	this.abbrevs["VL"]="VL";
	this.abbrevs["VIS"]="VIS";
	this.abbrevs["VIST"]="VIS";
	this.abbrevs["VISTA"]="VIS";
	this.abbrevs["VST"]="VIS";
	this.abbrevs["VSTA"]="VIS";
	this.abbrevs["WALK"]="WALK";
	this.abbrevs["WALKS"]="WALK";
	this.abbrevs["WALL"]="WALL";
	this.abbrevs["WY"]="WAY";
	this.abbrevs["WAY"]="WAY";
	this.abbrevs["WAYS"]="WAYS";
	this.abbrevs["WELL"]="WL";
	this.abbrevs["WELLS"]="WLS";
	this.abbrevs["WLS"]="WLS";

	this.suffixAbbrev2 = {};
	this.suffixAbbrev2["APARTMENT"]="APT";
	this.suffixAbbrev2["BASEMENT"]="BSMT";
	this.suffixAbbrev2["BUILDING"]="BLDG";
	this.suffixAbbrev2["DEPARTMENT"]="DEPT";
	this.suffixAbbrev2["FLOOR"]="FL";
	this.suffixAbbrev2["FRONT"]="FRNT";
	this.suffixAbbrev2["HANGERr"]="HNGR";
	this.suffixAbbrev2["KEY"]="KEY";
	this.suffixAbbrev2["LOBBY"]="LBBY";
	this.suffixAbbrev2["LOT"]="LOT";
	this.suffixAbbrev2["LOWNER"]="LOWR";
	this.suffixAbbrev2["OFFICE"]="OFC";
	this.suffixAbbrev2["PENTHOUSE"]="PH";
	this.suffixAbbrev2["PIER"]="PIER";
	this.suffixAbbrev2["REAR"]="REAR";
	this.suffixAbbrev2["ROOM"]="RM";
	this.suffixAbbrev2["SIDE"]="SIDE";
	this.suffixAbbrev2["SLIP"]="SLIP";
	this.suffixAbbrev2["SPACE"]="SPC";
	this.suffixAbbrev2["STOP"]="STOP";
	this.suffixAbbrev2["SUITE"]="STE";
	this.suffixAbbrev2["TRAILER"]="TRLR";
	this.suffixAbbrev2["UNIT"]="UNIT";
	this.suffixAbbrev2["UPPER"]="UPPR";
	
	this.geodirs = {};
	this.geodirs["NORTH"]="N";
	this.geodirs["EAST"]="E";
	this.geodirs["SOUTH"]="S";
	this.geodirs["WEST"]="W";
	this.geodirs["NORTHEAST"]="NE";
	this.geodirs["NORTH EAST"]="NE";
	this.geodirs["SOUTHEAST"]="SE";
	this.geodirs["SOUTH EAST"]="SE";
	this.geodirs["NORTHWEST"]="NW";
	this.geodirs["NORTH WEST"]="NW";
	this.geodirs["SOUTHWEST"]="SW";
	this.geodirs["SOUTH WEST"]="SW";
	

	this.AdjustStreet = function(street) {
		street = street.split(',').shift();
		street = street.trim();
		street = street.replace(/\./g,'');
		
		
		console.log(street);
		segs = street.split(" ");
		
		for(var i in segs) {
			var seg = segs[i];
			seg = seg.toUpperCase();
			if(this.abbrevs.hasOwnProperty(seg)) {
				segs[i] = this.abbrevs[seg.toUpperCase()];
			} else if(this.suffixAbbrev2.hasOwnProperty(seg)) {
				segs[i] = this.suffixAbbrev2[seg];
			} else if(this.geodirs.hasOwnProperty(seg)) {
				segs[i] = this.geodirs[seg];
			};	
		}
		return segs.join(" ");
	};
};
