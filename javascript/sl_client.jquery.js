/**
 * 
 */
(function($) {
	SimpleLayers = {
		baseURL : "https://secure.simplelayers.com/",
		wapiURL : "https://dev.simplelayers.com/~art/simplelayers/wapi/",
		token : null,
		application : null,
		connectHandler : null,
		Connect : function(application, handler) {
			if ((typeof application === 'undefined')) {
				throw Error('SimpleLayers().Connect requires an application');
			}
			if ((typeof handler === 'undfined') || handler == null)
				handler = this.OkHandler.bind(this);

			this.connectHandler = handler.bind(this);
			this.application = application;
			var endpoint = 'auth/authenticate';
			var params = {};
			params.application = application;
			params.format = 'json';
			return this.Request(endpoint, params, this.AuthHandler.bind(this));

		},
		AuthHandler : function(data) {
			this.token = data.token;
			if (this.connectHandler !== null)
				this.connectHandler(data);

		},
		
		PostEndPoint:function(endPoint,params,okHandler,resultsOnly) {
			this.Request(endPoint,params,okHandler,resultsOnly);
		},
		
		Request : function(endPoint, params, okHandler, resultsOnly) {
			params.format = "json";
			if (typeof resultsOnly === 'undefined')
				resultsOnly = true;
			if ((typeof okHandler === 'undfined') || okHandler == null)
				okHandler = this.OkHandler.bind(this);
			if (this.token !== null)
				params['token'] = this.token;
			if (endPoint.substr(0, 1) == '/')
				endPoint = endPoint.substr(1);
			if (endPoint.substr(-1) !== '/')
				endPoint += '/';
			endPoint = this.wapiURL + endPoint;
			$.post(endPoint, params, function(data) {
				if (resultsOnly) {
					if(data.hasOwnProperty('results')) {
						if(data.results.hasOwnProperty('results')){
							okHandler(data.results.results);
						} else {
							okHandler(data.results);
						}
					} else {
						okHandler(data);
					}					
				} else {
					okHandler(data)
				}
			}, 'json');
			return this;
		},

		OkHandler : function(data) {
			console.log(event);
		},

		ProblemHandler : function(data) {
			console.log(event);
		}
	};

}(jQuery));