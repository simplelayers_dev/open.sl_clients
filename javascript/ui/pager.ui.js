/**
 * 
 */
UI = (typeof UI == 'undefined') ? {} : UI;

UI.Pager = function(container, pagerId,position) {
	this.PGRSTATE_LOADING = "loading";
	this.PGRSTATE_NO_RESULTS = "no_results";
	this.PGRSTATE_PROBLEM = "problem";
	this.PGRSTATE_READY = "ready";
	this.PGRSTATE_NONE = "none";

	this._pagerStates = [ this.PGRSTATE_LOADING, this.PGRSTATE_NO_RESULTs,
			this.PGRSTATE_NO_PROBLEM, this.PGRSTATE_READY, this.PGRSTATE_NONE ];
	this._pagerStateDefault = this.PGRSTATE_NONE;
	this._pagerState = this._pagerStateDefault;

	this.SetPagerState = function(state) {
		this._pagerState = this._pagerStateDefault;
		if (this.pager !== null) {
			for ( var i in this._pagerStates) {
				this.pager.toggleClass('pager-' + this._pagerStates[i],
						this._pagerStates[i] == state);
				for( var l in this._labels) {
					this._labels[l].toggleClass('hidden',this._labels[l].hasClass('req-'+state));
				}
				if (this._pagerStates[i] == state)
					this._pagerState = state;
			}
		}
			
	};

	
	this.PGNGSTATE_START = "start";
	this.PGNGSTATE_MIDDLE = "middle";
	this.PGNGSTATE_END = "end";
	this.PGNGSTATE_NONE = "none";
	
	this._pagingStates = [ this.PGNGSTATE_START, this.PGNGSTATE_MIDDLE,
			this.PGNGSTATE_END, this.PGNGSTATE_NONE ];
	this._pagingStateDefault = this.PGNGSTATE_NONE;

	this.SetPagingState = function(state) {
		this._pagingState = this._pagingStateDefault;
		if (this.pager !== null) {
			for ( var i in this._pagerStates) {
				this.pager.toggleClass('paging-' + this._pagingStates[i],
						this._pagerStates[i] == state);
				if (this._pagingStates[i] = state)
					this._pagingState = state;
			}
		}
		switch (this._pagingState) {
		case this.PGNGSTATE_NONE:
			this._prev_btn.toggle('hidden', true);
			this._next_btn.toggle('hidden', true);
			break;
		case this.PGNGSTATE_START:
			this._prev_btn.toggle('hidden', false);
			this._next_btn.toggle('hidden', true);
			break;
		case this.PGNGSTATE_MIDDLE:
			this._prev_btn.toggle('hidden', false);
			this._next_btn.toggle('hidden', false);
			break;
		case this.PGNGSTTE_END:
			this._prev_btn.toggle('hidden', true);
			this._next_btn.toggle('hidden', false);

			break;
		}
	};

	this._pagerId = pagerId;
	this._container = container;
	this._position = position;
	
	this._labels = [];
	
	this._template = "<div id=\"{pagerId}\" class=\"pager\">";
	this._template += "<label id=\"{pagerId}\" class=\"stateful req-ready\">Displaying items ";
	this._template += "<span id=\"{pagerId}_from_lbl\"></span> to ";
	this._template += "<span id=\"{pagerId}_to_lbl\"></span> of ";
	this._template += "<span id=\"{pagerId}_count_lbl\"></span>";
	this._template += "<button id=\"{pagerId}_prev_btn\" type=\"button\" class=\"btn-sm gradient btn-primary\">&lt;&lt;</button>";
	this._template += "<button id=\"{pagerId}_next_btn\" type=\"button\" class=\"btn-sm gradient btn-primary\">&lt;&lt;</button>";
	this._template += "</label>";
	this._template += "<label class=\"stateful req-"+this.PGRSTATE_NO_RESULTS+"\">No results found</label>";
	this._template += "<label class=\"stateful req-"+this.PGRSTATE_LOADING+"\"></label>";
	this._template += "</div>";

	this._pager = "";
	this.pager = null;
	this._nextVal = null;
	this._prevVal = null;
	this._from_lbl = null;
	this._to_lbl = null;
	this._count_lbl = null;
	this._next_btn = null;
	this._prev_btn - null;
	this._nextHandler = null;
	this._prevHandler = null;

	this.SetupPager = function() {
		this._pager = this._template.substr(0); // get a copy of template.
		this._SetTemplateValue('pagerId', this._pagerId);
		this.pager = $.parseHTML(this._pager);
		switch(this.position) {
		case 'first':
			$(this._container).prepend(this.pager);
			break;
		case 'last':
			$(this._container).append(this.pager);
			break;
		}
		
		
		this._from_lbl = $('#' + this._pagerId + '_from_lbl');
		this._to_lbl = $('#' + this._pagerId + '_from_lbl');
		this._count_lbl = $('#' + this._pagerId + '_count_lbl');
		this._next_btn = $('#' + this._pagerId + '_next_btn');
		this._prev_btn = $('#' + this._pagerId + '_prev_btn');
		this._prev_btn.toggleClass('hidden', true);
		this._prev_btn.toggleClass('hidden', true);
		this._labels = this.pager.find('label.stateful');
		this._labels.toggleClass('stateful',false);

	};

	this.SetLabelVal = function(target, info, prop, val) {
		val = info[prop];
		if (val === null)
			val = "";
		if (val === undefined)
			val = "";
		if (val < 0)
			val = "";
		target.val(val);
	};

	this.SetPagerInfo = function(info, prevHandler, nextHandler) {
		if (this._prevHandler) {
			if (this._prevHandler !== prevHandler) {
				this._prev_btn.off('click');
				this._prevHandler = prevHandler;
			}
		}
		if (this._nextHandler) {
			if (this._nextHandler !== nextHandler) {
				this._next_btn.off('click');
				this._nextHandler = nextHandler;
			}
		}
		this._nextVal = info.hasOwnProperty('next') ? info.next : null;
		this._prevVal = info.hasOwnProperty('prev') ? info.prev : null;

		this._prev_btn.on('click', this._prevHandler);
		this._next_btn.on('click', this._nextHandler);

		if (info.hasOwnProperty('limit')) {
			if (info.limit == -1) {
				this._prev_btn.toggleClass('hidden', true);
				this._prev_btn.toggleClass('hidden', true);
				var calcdInfo = {
					"from" : 0,
					"to" : info.count,
					"count" : info.count
				};
				this.SetLabelVal(this.from_lbl, calcdInfo, 'from');
				this.SetLabelVal(this.to_lbl, calcdOnfo, 'to');
				this.SetLabelVal(this.count_lbl, calcdInfo, 'count');
				if (info.prev == 0 && (info.next == "")) {
					this.SetPagingState(this.PGNGSTATE_NONE);
				} else if (info.prev > 0 && info.next != "") {
					this.SetPagingState(this.PGNGSTATE_START);
				} else if (info.prev > 0 && info.next < info.count) {
					this.SetPagingState(this.PGNGSTATE_MIDDLE);
				} else if (info.prev > 0 && info.next >= info.count) {
					this.SetPagingState(this.PGNGSTATE_END);
				}

			} else {
				this.SetPagingState = this.PGNGSTATE_NONE;
				this.SetLabelVal(this.from_lbl, calcdInfo, 'from');
				this.SetLabelVal(this.to_lbl, calcdOnfo, 'to');
				this.SetLabelVal(this.count_lbl, calcdInfo, 'count');
			}

		}
	};

	this._SetTemplateValue = function(needle, value) {
		while (this._pager.indexOf('{' + needle + '}') > -1) {
			this._pager = this._table.replace('{' + needle + '}', value);
		}
	};

	this.SetupPager();

};