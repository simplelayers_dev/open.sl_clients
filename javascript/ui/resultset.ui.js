/**
 * 
 */
UI = (typeof UI == 'undefined') ? {} : UI;

UI.ResultSet = function() {
	this.STATE_NONE = 'none';
	this.STATE_LOADING = 'loading';
	this.STATE_LOADED = 'loaded';
	this.STATE_NODATA = 'no-data';
	this._states = [ this.STATE_NONE, this.STATE_LOADING, this.STATE_LOADED,
			this.STATE_NODATA ];

	this._template = '<div class="result row">';
	this._template += '<label class="listing-title">{layerName}</label>';
	this._template += '<table class="resultset" id="{mapLayerId}-listing" data-map-layer-id="{mapLayerId}" data-layer-id="{layerId}">';
	this._template += '<thead id="{mapLayerId}_tHeader">';
	this._template += '<tr><th><input id="{mapLayerId}_all_cb" type="checkbox" ></input></th></tr>';
	this._template += '</thead>';
	this._template += '<tbody id="{mapLayerId}_tBody"></tbody>';
	this._template += '<tfoot id="{mapLayerId}_tFoot"></tfoot>';
	this._template += '</table>';
	this._template += '</div>';

	this._container = null;
	this._table = "";
	this._idPrefix = null;
	this._tHead = null;
	this._tBody = null;
	this._tFoot = null;
	this._all_cb = null;
	this._results = null;
	this._selectedItems = null;
	this._unselectedItems = null;
	this._idField = 'gid';

	this.listing = null;

	this.layerInfo = null;
	this.SetLayerInfo = function(layerInfo, opt_idField, opt_showBeforeLoaded,
			opt_state) {
		if (opt_showBeforeLoaded === undefined) {
			opt_showBeforeLoaded = true;
		}

		if (opt_state === undefined) {
			opt_state = opt_showBeforeLoaded ? this.STATE_LOADING : this.STATE_NONE;
		}
		this._showBeforeLoaded = opt_showBeforeLoaded;
		this.layerInfo = layerInfo;
		this._idPrefix = this.layerInfo.Get('mapLayerId') + '_';
		this._idField = opt_idField;
		if (opt_state === undefined)
			state = this.STATE_LOADING;
		if (opt_showBeforeLoaded === undefined)
			showBeforeLoaded = true;

		this._table = this._template.substr(0);
		this._SetTemplateValue('mapLayerId', layerInfo.Get('mapLayerId'));
		this._SetTemplateValue('layerId', layerInfo.Get('layerId'));
		this._SetTemplateValue('layerName', layerInfo.Get('name'));
		if (opt_showBeforeLoaded) {

			this._MakeTable();
			this.SetState(opt_state);
		}
		return this;
	};

	this._SetTemplateValue = function(needle, value) {
		while (this._table.indexOf('{' + needle + '}') > -1) {
			this._table = this._table.replace('{' + needle + '}', value);
		}
	};

	this.SetContainer = function(containerElement) {
		this._container = containerElement;
		return this;
	};

	this._MakeTable = function() {
		if (this._table !== null) {

			if (this._container !== null) {
				console.log(this._table);
				this.listing = $.parseHTML(this._table);

				$(this._container).append(this.listing);
				this._tHead = $('#' + this._idPrefix + 'tHeader');
				this._tBody = $('#' + this._idPrefix + 'tBody');
				this._tFoot = $('#' + this._idPrefix + 'tFoot');

			}
			this._SetHeaderRow.call(this);
		}
		return this;
	};

	this.HandleAllCB = function(event) {
		var cbs = this._tBody.find('.' + this._idPrefix + '-cb');
		cbs.prop('checked', this._all_cb.prop('checked'));
		this.UpdateSelections();
	};

	this.HandleCB = function(event) {
		this.UpdateSelections();
	};

	this.UpdateSelections = function() {
		this._selectedItems = [];
		this._unselectedItems = [];
		var cbs = $('.' + this._idPrefix + '-cb');
		for ( var i =0,l=cbs.length;i<l;i++) {
			var cb = cbs[i];
			if ($(cb).prop('checked')) {
				this._selectedItems.push(this._results[$(cb).val()][this._idField]);
			} else {
				this._unselectedItems.push(this._results[$(cb).val()][this._idField]);
			}
		}
	};

	this.GetResults = function(which, opt_original) {
		var resultSet = null;

		if (opt_original === undefined)
			opt_original = false;
		switch (which) {
		case 'all':
			if (!opt_original) {
				resultSet = this._results.slice(0);
			} else {
				resultSet = this._results;
			}
			break;
		case 'selected':
			if (this._selectedItems.length == 0) {
				return [];
			}
			for ( var r in this._results) {
				var result = this._results[r];
				if (this._selectedItems.indexOf(this._results[this._idField]) > -1) {
					resultSet.push(result);
				}

			}
			break;
		case 'unselected':
			if (this._unselectedItems.length == 0) {
				return [];
			}
			for ( var r in this._results) {
				var result = this._results[r];
				if (this._unselectedItems.indexOf(this._results[this._idField]) > -1) {
					resultSet.push(result);
				}

			}

			break;
		return resultSet;

	}
}	;

	this._SetHeaderRow = function() {

		var attData = this.layerInfo.GetSearchableAttributes();

		var row = $(this._tHead.find('tr'));
		this._all_cb = $('#' + this._idPrefix + 'all_cb');
		
		$(row).find('.'+this._idPrefix+'th').remove();
		
		
		for ( var i in attData) {
			var att = attData[i];
			var label = att.display;
			$(row).append($.parseHTML('<th class="'+this._idPrefix+'th">' + label + '</th>'));
		}
		this._all_cb.on('click', this.HandleAllCB.bind(this));

	};

	this.AddResult = function(result, index) {
		var atts = this.layerInfo.GetSearchableAttributes();

		var row = $.parseHTML('<tr></tr>');
		this._tBody.append(row);
		var cb_input = $.parseHTML('<td><input type="checkbox" title="Select/Deselect this entry" class="'
						+ this._idPrefix + '-cb" value="' + index + '"></input></td>');
		$(row).append(cb_input);

		for ( var a in atts) {
			var att = atts[a];
			if(result[att.name]=='') result[att.name] = '&nbsp;';
			var	label = result.hasOwnProperty(att.name) ? result[att.name] : '&nbsp';
			
			var isURL = (att.type == 'cg_url');
			if(!isURL) isURL = (att.name == 'hyperlink');
			if(!isURL) {
				if(label) {
					if(label.hasOwnProperty('indexOf')) {
						isURL = (label.indexOf('http')==0);
					}
				}
			}
			
			if(label !== '&nbsp') {
				if(isURL) {
					if(label.indexOf(':')>-1) {
						var ending = label.split('/').pop();
						label = '<a target="_blank" href="'+result[att.name]+'">'+ending+'</a>'
					}
				}
			}			
			$(row).append($.parseHTML('<td>' + label + '</td>'));

			$('.' + this._idPrefix + '-cb').prop('checked', true);
			$('.' + this._idPrefix + '-cb').on('click', this.HandleCB.bind(this));

		}
		$(row).find(':last-child').toggleClass('absorbing-col', true);
	};

	this.HandleResults = function(results) {
		this._results = results;
		$(this._tBody).empty();
		if (results.length == 0) {
			if (this._showBeforeLoaded === false) {
				return;
			} else {
				this.Remove();
			}
		}
		if (this._tBody === null)
			this._MakeTable();

		for ( var r in results) {
			var result = results[r];
			this.AddResult(result, r);
		}

		if (results.length == 0) {
			this.SetState(this.STATE_NODATA);
			return;
		}
		if (results == false) {
			this.SetState(this.STATE_NODATA);
			return;
		}
		setTimeout(this.UpdateHeaders.bind(this), 20);
		this.SetState(this.STATE_LOADED);
	};

	this.UpdateHeaders = function() {
		return;
		var firstResult = this._tBody.find('tr')[0];
		var thElements = this._tHead.find('th');
		$(firstResult).find('td').each(function(index, element) {
			$(thElements[index]).width($(element).width());
		});

	};

	this.SetState = function(state) {
		if (this._mode === this.STATE_NONE) {
			if (this._mode !== mode) {
				this._MakeTable();
			}
		}
		switch (state) {
		case this.STATE_LOADING:
			if (this._tBody === null)
				return;
			this._tBody.empty();
			this._tFoot.empty();
			this._tBody.append('<tr><td colspan="'
					+ this.layerInfo.GetSearchableAttributes().length
					+ '">Searching for matches</td>');

			break;
		case this.STATE_LOADED:
			break;
		case this.STATE_NODATA:
			if (this._tBody === null)
				return;
			this._tBody.empty();
			this._tFoot.empty();
			this._tBody.append('<tr><td colspan="'
					+ this.layerInfo.GetSearchableAttributes().length
					+ '">No Matches Found</td></tr>');
			break;
		case this.STATE_NONE:
		default:
			if (this._tBody === null)
				return;
			this._tBody.empty();
			break;
		}
		for ( var i in this._states) {
			var stateEntry = this._states[i];
			$(this._container).toggleClass('listing-' + stateEntry,
					stateEntry == state);
		}

	};
	this.Remove = function() {
		$(this.listing).remove();
	}
};