/**
 * 
 * Created on Jan 21, 2018
 * 
 * @author: Arthur
 */
SimpleLayers.SLMapInfo = function(sl_client) {
	var self = this;
	this.client = sl_client;
	this.mapId = null;

	this.data = null;

	this.LoadMap = function(mapId, loadedHandler) {
		this.mapId = mapId;
		var params = {};
		params["mapId"] = mapId;
		this.client.Request("map/load/", params, (function(data) {
			this.MapLoaded(data);
			loadedHandler({
				event : 'MapLoaded',
				map : this
			});
		}).bind(this));
	};

	this.MapLoaded = function(data) {
		this.SetData(data.project);
	};

	this.SetData = function(data) {
		this.data = data;
	};

	this.GetLayers = function() {
		return new SimpleLayers.Layers(this.data.layers);
	};

};

SimpleLayers.Layer = function(layerInfo) {
	this.data = layerInfo;

	this.GetSearchableAttributes = function(opt_visOnly) {
		if(typeof(opt_visOnly) === 'undefined') {
			opt_visOnly = true;
		}
		var atts = [];
		for ( var i in this.data.layerAttributes) {
			var att = this.data.layerAttributes[i];
			if(att.visible == "1") {
				if(!opt_visOnly) continue;
			} 
			
			if (att.searchable == "1")
				atts.push(att);
		}
		return atts;
	};
	
	this.HasAttribute = function(attributeName) {
		for( var i in this.data.layerAttributes) {
			if( this.data.layerAttributes[i].name == attributeName) {
				return true;
			}			
		}
		return false;
	};

	this.Get = function(what) {
		if (this.hasOwnProperty('_get_' + what)) {
			if ((typeof this['_get_' + what]) === 'function') {
				var f = this['_get_' + what];
				return f.call(this);
			}
		}
		if (!this.hasOwnProperty('data'))
			return null;
		if (!this.data.hasOwnProperty(what))
			return null;
		return this.data[what];
	};
	
	this._get_mapLayerId = function() {
		return this.data['plid'];
	};
	
	this._get_layerId = function() {
		return this.data['id'];
	};

};

SimpleLayers.Layers = function(layers) {
	this.LAYER_TYPE_NONE = 0;
	this.LAYER_TYPE_VECTOR = 1;
	this.LAYER_TYPE_RASTER = 2;
	this.LAYER_TYPE_WMS = 3;
	this.LAYER_TYPE_ODBC = 4;
	this.LAYER_TYPE_RELATIONAL = 5;
	this.LAYER_TYPE_COLLECTION = 6;
	this.LAYER_TYPE_SMART_LAYER = 7;
	this.LAYER_TYPE_RELATABLE = 8;

	this.layers = layers;

	this.GetLayers = function(layerType, unique, searchable) {
		if (layerType === undefined)
			layerType = null;
		if (unique === undefined)
			unique = false;
		if (searchable === undefined)
			searchable = false;

		var uniqueIds = [];
		var layers = [];
		for ( var layerId in this.layers) {
			var layer = this.layers[layerId];

			if (layerType !== null) {
				if (parseInt(layer.type) != layerType)
					continue;
			}
			if (unique) {
				if (uniqueIds.indexOf(layerId) > -1)
					continue;
			}
			if (searchable) {
				if (layer.search_on != 1)
					continue;
			}
			layers.push(layer);
			if (unique) {
				uniqueIds.push(layerId);
			}
			;
		}
		return layers;
	};

};